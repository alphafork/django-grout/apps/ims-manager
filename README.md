# IMS Manager

App for Manager staff role in dj-ims.


## License


  [AGPL-3.0-or-later](LICENSE)



## Contact

Alpha Fork Technologies

[connect@alphafork.com](mailto:connect@alphafork.com)
