from django.apps import AppConfig


class IMSManagerConfig(AppConfig):
    default_auto_field = "django.db.models.BigAutoField"
    name = "ims_manager"
