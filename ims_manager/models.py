from django.db import models
from reusable_models import get_model_from_string

StaffDesignation = get_model_from_string("STAFF_DESIGNATION")
Designation = get_model_from_string("DESIGNATION")


class ManagerManager(models.Manager):
    def get_queryset(self):
        return super().get_queryset().filter(designation__name="Manager")


class Manager(StaffDesignation):
    objects = ManagerManager()

    class Meta:
        proxy = True
